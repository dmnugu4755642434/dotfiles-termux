# Setup

## Copy content to termux home folder

```sh
cp -r ./home/* ~
cp -r ./home/.* ~
```

## Run setup script

**The the executable bit is set on `setup.sh` for it to run**

```sh
cd ~ && chmod +x setup.sh && ./setup.sh
```

**"home/.tmux/plugins/tpm" contains a stripped down version of the TMUX plugin
manager repository**
