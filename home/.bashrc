#
# ~/.bashrc
#

# Sources
# ----
# . sth or source sth
[[ -r ~/.aliases ]] && source ~/.aliases
[[ -r ~/.functions ]] && . ~/.functions

if [[ -d "$ROOT/usr/share/fzf" ]]; then
	. "$ROOT/usr/share/fzf/key-bindings.bash" 2>/dev/null
	. "$ROOT/usr/share/fzf/completion.bash"
fi

[[ -r "$ROOT"/usr/share/doc/pkgfile/command-not-found.bash ]] && . "$ROOT"/usr/share/doc/pkgfile/command-not-found.bash
# ----

# Shell options
# ----
set -o vi               # VI mode
shopt -s autocd         # cd into a type directory
shopt -s cdable_vars    # Assume invalid cd argument is variable
shopt -s cdspell        # Correct cd spelling error
shopt -s checkwinsize   # Autoupdate window size
shopt -s cmdhist        # Save multiline commands as single in history
shopt -s histappend     # don't overwrite history
shopt -s dotglob        # Include dotfiles in pathname expansion
shopt -s expand_aliases # Expand aliases
shopt -s extglob        # Enable extended globbing
shopt -s globstar       # Enable recursive globbing
shopt -s hostcomplete   # Attempt hostname expansion on "@"

#shopt -s progcomp           # programmable completion
#
[[ -z "$HISTFILE" ]] && HISTFILE=~/.bash_history

export HISTSIZE=10500
export SAVEHIST=10000
export HISTCONTROL=ignoreboth:erasedups
# ----

# Completions
# ----
# pip bash completion
_pip_completion() {
	COMPREPLY=(
		$(COMP_WORDS="${COMP_WORDS[*]}" \
			COMP_CWORD=$COMP_CWORD \
			PIP_AUTO_COMPLETE=1 $1))
}
complete -o default -F _pip_completion pip
# ----

# ----
[[ -r ~/.postinits ]] && . ~/.postinits

[[ -x "$(command -v starship)" ]] &&
     eval "$(starship init bash)"
# ----
