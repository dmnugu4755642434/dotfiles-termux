#!/bin/bash

validate_setup_files() {
	[[ -d "$HOME/setup.d" ]] && return 0

	printf "Copy the \"setup.d\" directory to the same path as this script then execute it\n"
	return 1
}

setup_its_pointless_repo() {
	# Setup its-pointless' termux repo.
	chmod +x ./setup.d/setup-pointless-repo.sh &&
		./setup.d/setup-pointless-repo.sh &&
		rm pointless.gpg*

	apt update
}

add_requisite_repos_packages() {
	# Add "root-repo", git, openssh & update repository cache.
	apt update &&
		apt install --assume-yes git openssh
}

setup_misc_packages() {
	# Install necessary packages.
	# shellcheck disable=SC2046
	apt install --assume-yes $(cat ./setup.d/apt.txt)

	# Install python packages
	pip install --user pipx &&
		pipx install thefuck

	install_termux_plugin_manager &

	wait
}

install_termux_plugin_manager() {
	# Install TMUX plugin manager.
	TMUX_PLUGINS_DIR="$HOME/.tmux/plugins"
	if [[ ! -x "$TMUX_PLUGINS_DIR"/tpm/tpm ]]; then
		mkdir -p "$TMUX_PLUGINS_DIR" &&
			cd "$TMUX_PLUGINS_DIR" &&
			git clone --depth 1 "https://github.com/tmux-plugins/tpm"
	fi
}

postinstall() {
	# change shell to zsh.
	[[ -x $(command -v zsh) ]] && chsh -s zsh
}

main() {
	validate_setup_files
	[[ $? -eq 1 ]] && return

	add_requisite_repos_packages
	setup_its_pointless_repo

	setup_misc_packages

	postinstall
}

if [[ $# -eq 0 ]]; then
	main
else
	$1
fi
