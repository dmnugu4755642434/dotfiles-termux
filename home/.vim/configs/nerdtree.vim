"
" ~/.vim/configs/nerdtree.vim
"

function NerdTreeStartup()
	" Gets rid off the error on startup
	if argc() == 0 && !exists('s:std_in')
		NERDTree
	endif
endfunction

function KillNerdTreeOnly()
	if (winnr('$') == 1 && exists('b:NERDTree') && b:NERDTree.isTabTree())
		qa
	endif
endfunction

" Autocommands need to be in a group.
augroup MyNerdtree
	" Open NerdTree when no file is selected
	autocmd StdinReadPre * let s:std_in = 1
	autocmd VimEnter * call NerdTreeStartup()

	" Close vim with single ':q' if NerdTree is the only thing open
	autocmd BufEnter * call KillNerdTreeOnly()

	" Don't open file in NerdTree window
	autocmd BufEnter * if bufname('#') =~# '^NERD_tree_' && winnr('$') > 1 | b# | endif
augroup END

" Sort files naturally: 1.txt, 2.txt, 10.txt.
let g:NERDTreeNaturalSort = 1

" Close after opening file
" let g:NERDTreeQuitOnOpen = 1

" NerdTree File highlighting.
function! NERDTreeHighlightFile(extension, fg, bg, guifg, guibg)
	exec 'autocmd filetype nerdtree highlight '. a:extension. ' ctermbg='. a:bg. ' ctermfg='. a:fg. ' guibg='. a:guibg. ' guifg='. a:guifg
	exec 'autocmd filetype nerdtree syn match '. a:extension. ' #^\s\+.*'. a:extension. '$#'
endfunction

" Cterm-colors (16-colours)
let g:my_cblue = 'Blue'
let g:my_cbrown = 'Brown'
let g:my_ccyan = 'Cyan'
let g:my_cgreen = 'Green'
let g:my_cmagenta = 'Magenta'
let g:my_cred = 'Red'
let g:my_cyellow = 'Yellow'

" Gui-colors (256-colour scheme)
let g:my_blue = '#3366ff'
let g:my_brown = '#f4a460'
let g:my_cyan = '#00ffff'
let g:my_green = '#85df2f'
let g:my_magenta = '#ff00ff'
let g:my_red = '#f712bb'
let g:my_yellow = '#ffa500'
let g:my_black = '#151515'

call NERDTreeHighlightFile('c',      g:my_cmagenta, 'NONE', g:my_magenta, 'NONE')
call NERDTreeHighlightFile('cmake',  g:my_cblue,    'NONE', g:my_blue,    'NONE')
call NERDTreeHighlightFile('coffee', g:my_cred,     'NONE', g:my_red,     'NONE')
call NERDTreeHighlightFile('conf',   g:my_cyellow,  'NONE', g:my_yellow,  'NONE')
call NERDTreeHighlightFile('cpp',    g:my_cmagenta, 'NONE', g:my_magenta, 'NONE')
call NERDTreeHighlightFile('cs',     g:my_cblue,    'NONE', g:my_blue,    'NONE')
call NERDTreeHighlightFile('cs',     g:my_cblue,    'NONE', g:my_blue,    'NONE')
call NERDTreeHighlightFile('css',    g:my_ccyan,    'NONE', g:my_cyan,    'NONE')
call NERDTreeHighlightFile('csv',    g:my_cgreen,   'NONE', g:my_green,   'NONE')
call NERDTreeHighlightFile('cxx',    g:my_cmagenta, 'NONE', g:my_magenta, 'NONE')
call NERDTreeHighlightFile('go',     g:my_cblue,    'NONE', g:my_blue,    'NONE')
call NERDTreeHighlightFile('h',      g:my_cmagenta, 'NONE', g:my_magenta, 'NONE')
call NERDTreeHighlightFile('hpp',    g:my_cmagenta, 'NONE', g:my_magenta, 'NONE')
call NERDTreeHighlightFile('hs',     g:my_cmagenta, 'NONE', g:my_magenta, 'NONE')
call NERDTreeHighlightFile('html',   g:my_cbrown,   'NONE', g:my_brown,   'NONE')
call NERDTreeHighlightFile('html',   g:my_cyellow,  'NONE', g:my_yellow,  'NONE')
call NERDTreeHighlightFile('ini',    g:my_cyellow,  'NONE', g:my_yellow,  'NONE')
call NERDTreeHighlightFile('java',   g:my_cred,     'NONE', g:my_red,     'NONE')
call NERDTreeHighlightFile('jl',     g:my_cmagenta, 'NONE', g:my_magenta, 'NONE')
call NERDTreeHighlightFile('js',     g:my_cbrown,   'NONE', g:my_brown,   'NONE')
call NERDTreeHighlightFile('json',   g:my_cyellow,  'NONE', g:my_yellow,  'NONE')
call NERDTreeHighlightFile('make',   g:my_cblue,    'NONE', g:my_blue,    'NONE')
call NERDTreeHighlightFile('md',     g:my_cblue,    'NONE', g:my_blue,    'NONE')
call NERDTreeHighlightFile('ninja',  g:my_cyellow,  'NONE', g:my_yellow,  'NONE')
call NERDTreeHighlightFile('org',    g:my_cyellow,  'NONE', g:my_yellow,  'NONE')
call NERDTreeHighlightFile('pdf',    g:my_cred,     'NONE', g:my_red,     'NONE')
call NERDTreeHighlightFile('php',    g:my_cmagenta, 'NONE', g:my_magenta, 'NONE')
call NERDTreeHighlightFile('py',     g:my_cyellow,  'NONE', g:my_yellow,  'NONE')
call NERDTreeHighlightFile('r',      g:my_cblue,    'NONE', g:my_blue,    'NONE')
call NERDTreeHighlightFile('rb',     g:my_cred,     'NONE', g:my_red,     'NONE')
call NERDTreeHighlightFile('rs',     g:my_ccyan,    'NONE', g:my_cyan,    'NONE')
call NERDTreeHighlightFile('s',      g:my_cblue,    'NONE', g:my_blue,    'NONE')
call NERDTreeHighlightFile('scala',  g:my_cred,     'NONE', g:my_red,     'NONE')
call NERDTreeHighlightFile('sh',     g:my_cgreen,   'NONE', g:my_blue,    'NONE')
call NERDTreeHighlightFile('ts',     g:my_cblue,    'NONE', g:my_blue,    'NONE')
call NERDTreeHighlightFile('txt',    g:my_cblue,    'NONE', g:my_blue,    'NONE')
call NERDTreeHighlightFile('vue',    g:my_cbrown,   'NONE', g:my_brown,   'NONE')
call NERDTreeHighlightFile('yaml',   g:my_cyellow,  'NONE', g:my_yellow,  'NONE')
call NERDTreeHighlightFile('yml',    g:my_cyellow,  'NONE', g:my_yellow,  'NONE')
