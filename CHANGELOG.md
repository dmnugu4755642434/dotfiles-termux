<a name=""></a>
##  (2021-04-12)


#### Performance

* ***:**  Add multiple changes ([a4874582](a4874582))
* **home/*:**  Replace powerlevel10k with starship ([9c906768](9c906768))

#### Features

* ***:**  Add initial configs ([ed3784b8](ed3784b8))
* **.tmux/plugins/tpm:**  Add stripped down copy of TPM's repo ([55a26ccc](55a26ccc))
* **home/.vimrc:**  Add support for additional LaTex's engines ([2d06812a](2d06812a))
* **home/setup.sh:**
  *  Switch from powerlevel9k to powerlevel10k ([db812c80](db812c80))
  *  Add `postinstall` function ([875816a2](875816a2))

#### Bug Fixes

* ***:**
  *  Move TMUX plugin manager setup to setup.sh ([24cf747a](24cf747a))
  *  Remove unnecessary fonts ([2b3f2e87](2b3f2e87))
* **.vim/config/coc.vim:**  Replace coc-python with coc-pyright ([0cae0bb6](0cae0bb6))
* **.vimrc:**  Change `UnicodeDownload` call to `DownloadUnicode` ([4e2ca3c3](4e2ca3c3))
* **home/*:**  Resolve setup issues ([d49619b7](d49619b7))
* **home/.*:**
  *  Alter file `[[` check flags ([405f137d](405f137d))
  *  Add missing zsh & tmux plugins ([3580d668](3580d668))
* **home/.aliases:**  Alter aliases to used non-aliased commands ([daa53019](daa53019))
* **home/.config/starship.toml:**  Alter starship's time module ([097e0b41](097e0b41))
* **home/.functions:**
  *  Remove erroneous flag from fortune call ([a8f70017](a8f70017))
  *  Guard `cow_list` population in `fortunecow` ([545638c2](545638c2))
  *  Add the ARRAY_START export for array indexing ([fa2a823a](fa2a823a))
* **home/.mpd/mpd.conf:**
  *  Change the playlist directory ([8401f216](8401f216))
  *  Change directory paths ([78bae66e](78bae66e))
* **home/.tmux.conf:**  Change tmux-yank's default clipboard ([5f1a629b](5f1a629b))
* **home/.vim/local-pl*:**  Remove hardlinks to gtags plugins ([2a376eb9](2a376eb9))
* **home/.vimrc:**  Fix `gpepprg` option ([0a63a03f](0a63a03f))



<a name=""></a>
##  (2021-04-01)


#### Bug Fixes

* ***:**
  *  Move TMUX plugin manager setup to setup.sh ([24cf747a](24cf747a))
  *  Remove unnecessary fonts ([2b3f2e87](2b3f2e87))
* **.vim/config/coc.vim:**  Replace coc-python with coc-pyright ([0cae0bb6](0cae0bb6))
* **.vimrc:**  Change `UnicodeDownload` call to `DownloadUnicode` ([4e2ca3c3](4e2ca3c3))
* **home/.*:**
  *  Alter file `[[` check flags ([405f137d](405f137d))
  *  Add missing zsh & tmux plugins ([3580d668](3580d668))
* **home/.aliases:**  Alter aliases to used non-aliased commands ([daa53019](daa53019))
* **home/.config/starship.toml:**  Alter starship's time module ([097e0b41](097e0b41))
* **home/.functions:**
  *  Remove erroneous flag from fortune call ([a8f70017](a8f70017))
  *  Guard `cow_list` population in `fortunecow` ([545638c2](545638c2))
  *  Add the ARRAY_START export for array indexing ([fa2a823a](fa2a823a))
* **home/.mpd/mpd.conf:**
  *  Change the playlist directory ([8401f216](8401f216))
  *  Change directory paths ([78bae66e](78bae66e))
* **home/.tmux.conf:**  Change tmux-yank's default clipboard ([5f1a629b](5f1a629b))
* **home/.vim/local-pl*:**  Remove hardlinks to gtags plugins ([2a376eb9](2a376eb9))
* **home/.vimrc:**  Fix `gpepprg` option ([0a63a03f](0a63a03f))

#### Features

* ***:**  Add initial configs ([ed3784b8](ed3784b8))
* **.tmux/plugins/tpm:**  Add stripped down copy of TPM's repo ([55a26ccc](55a26ccc))
* **home/.vimrc:**  Add support for additional LaTex's engines ([2d06812a](2d06812a))
* **home/setup.sh:**
  *  Switch from powerlevel9k to powerlevel10k ([db812c80](db812c80))
  *  Add `postinstall` function ([875816a2](875816a2))

#### Performance

* ***:**  Add multiple changes ([a4874582](a4874582))
* **home/*:**  Replace powerlevel10k with starship ([9c906768](9c906768))



<a name=""></a>
##  (2020-11-18)


#### Features

* ***:**  Add initial configs ([ed3784b8](ed3784b8))
* **.tmux/plugins/tpm:**  Add stripped down copy of TPM's repo ([55a26ccc](55a26ccc))
* **home/setup.sh:**
  *  Switch from powerlevel9k to powerlevel10k ([db812c80](db812c80))
  *  Add `postinstall` function ([875816a2](875816a2))

#### Bug Fixes

* ***:**
  *  Move TMUX plugin manager setup to setup.sh ([24cf747a](24cf747a))
  *  Remove unnecessary fonts ([2b3f2e87](2b3f2e87))
* **.vimrc:**  Change `UnicodeDownload` call to `DownloadUnicode` ([4e2ca3c3](4e2ca3c3))
* **home/.*:**
  *  Alter file `[[` check flags ([405f137d](405f137d))
  *  Add missing zsh & tmux plugins ([3580d668](3580d668))
* **home/.aliases:**  Alter aliases to used non-aliased commands ([daa53019](daa53019))
* **home/.functions:**
  *  Remove erroneous flag from fortune call ([a8f70017](a8f70017))
  *  Guard `cow_list` population in `fortunecow` ([545638c2](545638c2))
  *  Add the ARRAY_START export for array indexing ([fa2a823a](fa2a823a))
* **home/.mpd/mpd.conf:**
  *  Change the playlist directory ([8401f216](8401f216))
  *  Change directory paths ([78bae66e](78bae66e))
* **home/.tmux.conf:**  Change tmux-yank's default clipboard ([5f1a629b](5f1a629b))
* **home/.vim/local-pl*:**  Remove hardlinks to gtags plugins ([2a376eb9](2a376eb9))
* **home/.vimrc:**  Fix `gpepprg` option ([0a63a03f](0a63a03f))

#### Performance

* ***:**  Add multiple changes ([a4874582](a4874582))
* **home/*:**  Replace powerlevel10k with starship ([9c906768](9c906768))
